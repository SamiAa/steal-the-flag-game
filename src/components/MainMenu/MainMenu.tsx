import React from 'react';
import './MainMenu.css';

type Props = {
    startGame: Function
};

function MainMenu(props: Props) {
    return (
        <div className="MainMenu">
            <h1>Main Menu</h1>
            <div>
                <button onClick={() => props.startGame()}>
                    Start Game
                    <span className="Dot-green"></span>
                </button>
                <button>
                    Options
                    <span className="Dot-green"></span>
                </button>
            </div>
        </div>
    );
}

export default MainMenu;