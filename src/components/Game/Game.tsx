import React from 'react';
import './Game.css';

type Props = {
    pauseGame: Function
}

function Game(props: Props) {
    document.addEventListener("keydown", function(event) {
        if (event.key === 'Escape') {
            props.pauseGame();
        }
    });

    return (
        <div className="Game">
            {/* <h1>Game</h1> */}
            <div className="Game-area"></div>
        </div>
    );
};

export default Game;