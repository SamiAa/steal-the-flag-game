import React from 'react';
import MainMenu from './components/MainMenu/MainMenu';
import Game from './components/Game/Game';
import './App.css';

function App() {
  let startGame = () => {
    document.querySelector(".MainMenu")?.setAttribute("style", "margin-top: calc(-92vh + 16px)");
  };

  let pauseGame = () => {
    document.querySelector(".MainMenu")?.setAttribute("style", "0");
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>Steal the Flag</div>
      </header>
      <div className="App-content">
        <MainMenu startGame={startGame} />
        <Game pauseGame={pauseGame} />
      </div>
      <footer className="App-footer"></footer>
    </div>
  );
}

export default App;
